import React from 'react';
import { Alert } from "react-bootstrap"

export default function ErrorComponent(props) {
    return (<Alert variant="danger" onClick={props.click} dismissible>
        <p>
            {props.message}
        </p>
    </Alert>);
}