import React, { useState } from 'react';
import mockedData from '../mocks/mockedData';
import { Form, Button } from 'react-bootstrap';
import { HotelOptionsComponent } from './HotelOptionsComponent';
import { DateComponent } from './DateComponent';
import ErrorComponent from './ErrorComponent';

function SearchComponent(props) {
  const [showErrors, setShowErrors] = useState(false);
  const [nights, setNights] = useState(30);
  const [hotelId, setHotel] = useState(mockedData.hotels[0].hotelId);
  const [dateSelected, setDateSelected] = useState();
  const [disabled, setDisabledButton] = useState(false);

  const handleSubmitForm = (e) => {
    e.preventDefault();
    if (!hotelId || !dateSelected || !nights) {
      setShowErrors('revise la seleccion');
      setDisabledButton(true);
      return;
    }

    props.search({ hotelId, dateSelected, nights});
  }

  const handleSelectorDaysChange = (e) => {
    setDisabledButton(false);
    if (e.target.value > 30 || e.target.value < 1) {
      setShowErrors('revise la seleccion de noches');
      return
    }

    setNights(e.target.value);
  }

  const showDays = () => {
    const maxDays = new Array(30);
    let render = [];
    for (let index = maxDays.length; index >= 1; index--) {
      render.push(<option key={index} value={index}>{index}</option>);
    }
    return render;
  }
  const handleDateChange = (data) => {
    setShowErrors(false);
    setDisabledButton(false);

    const dateNow = new Date();
    if (dateNow.getTime() >= data.getTime()) {
      setShowErrors('la fecha no puede ser inferior al día de hoy');
      return;
    }

    setDateSelected(data);
  }

  return (
    <div>
    {showErrors && <ErrorComponent message={showErrors} click={() => {setShowErrors(false)}}/>}
    <Form onSubmit={handleSubmitForm}>
      <Form.Group controlId="selectedHotel">
        <Form.Label>Choose Hotel</Form.Label>
        <Form.Control as="select" onChange={ (e) => setHotel(e.target.value) }>
          <HotelOptionsComponent />
        </Form.Control>
      </Form.Group>
      <Form.Group>
        <Form.Label>Check in Date</Form.Label>
        <DateComponent change={ handleDateChange }/>
      </Form.Group>
      <Form.Group controlId="daysSelected">
        <Form.Label>Choose number of Nights</Form.Label>
        <Form.Control as="select" onChange={ handleSelectorDaysChange }>
          { showDays() }
        </Form.Control>
      </Form.Group>
      <Button variant="primary" type="submit" disabled={ disabled }>
        Search
      </Button>
    </Form>
    </div>
  )
}

export default SearchComponent;