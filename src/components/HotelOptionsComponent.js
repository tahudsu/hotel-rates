import React from 'react';
import { useEffect, useState } from "react";
import mockedData from "../mocks/mockedData";

export function HotelOptionsComponent() {
  const [hotels, setHotels] = useState('');
  useEffect(() => {
    async function getRates() {
      setHotels({
        ...mockedData.hotels
      });
    }
    getRates();
  }, []);

  return Object.keys(hotels).map((key) => {
        const hotel = hotels[key];
        return <option key={hotel.hotelId} id={hotel.hotelId} value={hotel.hotelId}>{ hotel.name }</option>
      });
}
