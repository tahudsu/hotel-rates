import React from 'react';
import ErrorComponent from './ErrorComponent';
import { Table } from 'react-bootstrap';

export function RatesList(props) {
    const [keyRate] = Object.keys(props.rates);
    const handleRates = () => {
        if (props.rates[keyRate]) {
            const show = [];

            for (const room of props.rates[keyRate]) {
                show.push(<tr key={room.rateCode}>
                    <td>{room.roomName}</td>
                    <td>{room.offerName || '-'}</td>
                    <td>{room.boardName}</td>
                    <td>{room.occupationDescription}</td>
                    <td>{room.netPrice}</td>
                </tr>);
            }
            const table =
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>habitacion</th>
                            <th>Oferta</th>
                            <th>Régimen</th>
                            <th>Ocupación</th>
                            <th>Precio Neto</th>
                        </tr>
                    </thead>
                    <tbody>
                        {show}
                    </tbody>
                </Table>
            return table;
        } else {
            const message = `Lo sentimos, no hay tarifas disponibles`;

            return <ErrorComponent message={message} />
        }
    }
    return (
        <div>
            {handleRates()}
        </div>
    );
}