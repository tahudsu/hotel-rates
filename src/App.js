import React, { useState } from 'react';
import logo from './logo.svg';
import './App.scss';
import { Jumbotron, Button } from 'react-bootstrap';
import SearchComponent from './components/SearchComponent';
import { hotels } from './services/hotels';
import { RatesList } from './components/RatesList';



function App() {
  const [availableRates, setAvailableRates ] = useState();
  const handleSearch = async (params) => {
    const data = await hotels.getRates(params);
    const { availableRates } = data.data;
    setAvailableRates(availableRates);
  }

  return (
    
      <div>
        <Jumbotron>
          <SearchComponent search={handleSearch} />
        </Jumbotron>
        { availableRates && <RatesList rates={availableRates} />}

      </div>
  );
}

export default App;
