const mockedData = {
    "hotels": [
        {
            "name": "Hotel Baqueira Val de Neu",
            "hotelId": 44069509
        },
        {
            "name": "Hotel Moderno",
            "hotelId": 10030559
        },
        {
            "name": "Hotel Grand Luxor",
            "hotelId": 100376478
        }
    ]
}

export default mockedData;