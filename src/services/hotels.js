import Axios from 'axios';

export const hotels = {
    getRates(params) {
        const { hotelId, dateSelected, nights } = params;
        const dateParsed = [dateSelected.getDate(), dateSelected.getMonth()+1, dateSelected.getFullYear()].join('/');
        const url = `https://api.mirai.com/MiraiWebService/availableRate/get?hotelId=${hotelId}&checkin=${dateParsed}&nights=${nights}&lang=es`;
        const auth = {
            auth: {
                username: 'user1',
                password: 'user1Pass'
              },
        }
        return Axios.get(url, auth);
    }
}